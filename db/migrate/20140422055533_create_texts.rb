class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.string :to
      t.string :message

      t.timestamps
    end
  end
end
