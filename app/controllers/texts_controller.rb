class TextsController < ApplicationController
  def new
    @text = Text.new
  end

  def create
    @text = Text.new(text_params)

    if @text.save
      # Instantiate a Twilio client
      @sender = Twilio::REST::Client.new(TWILIO_CONFIG['sid'], TWILIO_CONFIG['token'])

      # Create and send an SMS message
      @sender.account.sms.messages.create(
        from: TWILIO_CONFIG['from'],
        to: @text.to,
        body: @text.message
      )

      redirect_to root_path
    else
      @sent = false
      render :new
    end
  end

  def receive
    from = params['From']
    message = params['Body']
  end

  def text_params
    params.require(:text).permit(:to, :message)
  end
end
