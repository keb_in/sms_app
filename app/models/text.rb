class Text < ActiveRecord::Base
  validates :to, :message, presence: true
end
